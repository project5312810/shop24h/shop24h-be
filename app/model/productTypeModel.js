// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderSchema từ class Schema
const productTypeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
    description: {
        type: String
    }
}, 
{
    timestamps: true
})

module.exports = mongoose.model("ProductType", productTypeSchema);