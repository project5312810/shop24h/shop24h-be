// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderSchema từ class Schema
const productSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique:true
    },
    description: {
        type: String
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: "ProductType",
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    },
}, 
{
    timestamps: true
})

module.exports = mongoose.model("Product", productSchema);