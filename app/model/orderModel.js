// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderSchema từ class Schema
const orderSchema = new Schema({
    orderDate: {
        type: Date,
        default: Date.now,
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
    }],
    cost: {
        type: Number,
        default:0
    }
}, 
{
    timestamps: true
})

module.exports = mongoose.model("Order", orderSchema);