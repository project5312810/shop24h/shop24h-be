// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderSchema từ class Schema
const orderDetailSchema = new Schema({
    product: {
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    quantity: {
        type: Number,
        default:0
    }
}, 
{
    timestamps: true
})

module.exports = mongoose.model("OrderDetail", orderDetailSchema);