// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import Module product Model
const productModel = require("../model/productModel");
const productTypeModel = require("../model/productTypeModel");

const createProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;

    if (!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }
    if (!body.description) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Description không hợp lệ"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductType is not valid"
        })
    }
    if (!body.imageUrl) {
        return response.status(400).json({
            status: "Bad Request",
            message: "imageUrl không hợp lệ"
        })
    }
    if (isNaN(body.buyPrice) || body.buyPrice < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "buyPrice không hợp lệ"
        })
    }
    if (isNaN(body.promotionPrice) || body.promotionPrice < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "promotionPrice không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newproduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
    }

    productModel.create(newproduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create product successfully",
            data: data
        })
    })
}

const getAllProduct = (request, response) => {
    const limit = request.query.limit;
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    productModel.find()
        .limit(limit)
        .populate('type')
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                data: data
            })
        })
}

const getProductById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    productModel.findById(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Product Type successfully",
            data: data
        })
    })
}

const updateProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productID không hợp lệ"
        })
    }

    if (body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }

    if (body.description !== undefined && body.description.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Description không hợp lệ"
        })
    }
    if (body.ProductType && !mongoose.Types.ObjectId.isValid(body.ProductType)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "ProductType không hợp lệ"
        })
    }
    if (body.imageUrl !== undefined && body.imageUrl.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "imageUrl không hợp lệ"
        })
    }
    if (body.buyPrice !== undefined && (isNaN(body.buyPrice) || body.buyPrice < 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "buyPrice không hợp lệ"
        })
    }
    if (body.promotionPrice !== undefined && (isNaN(body.promotionPrice) || body.promotionPrice < 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "promotionPrice không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateproduct = {}

    if (body.name !== undefined) {
        updateproduct.name = body.name
    }

    if (body.description !== undefined) {
        updateproduct.description = body.description
    }
    if (body.ProductType !== undefined) {
        updateproduct.type = body.ProductType
    }
    if (body.imageUrl !== undefined) {
        updateproduct.imageUrl = body.imageUrl
    }
    if (body.buyPrice !== undefined) {
        updateproduct.buyPrice = body.buyPrice
    }
    if (body.promotionPrice !== undefined) {
        updateproduct.promotionPrice = body.promotionPrice
    }

    productModel.findByIdAndUpdate(productId, updateproduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Product Type successfully",
            data: data
        })
    })
}

const deleteProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete product successfully"
        })
    })
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct
}