// Import thư viện Mongoose
const { request, response } = require("express");
const mongoose = require("mongoose");
// Import Module product Model
const orderDetailModel = require("../model/orderDetailModel");
const orderModel= require("../model/orderModel");
const productModel = require("../model/productModel");

const createOrderDetailOfOrder=(request,response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId=request.params.orderId;
    const body = request.body;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order ID is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.product)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Product is not valid"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        product: body.product
    }

    orderDetailModel.create(newOrderDetail,(error,data)=>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        
        orderModel.findByIdAndUpdate(orderId,{
            $push:{orderDetails: data._id}
        },(err,updateOrder)=>{
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status: "Create OrderDetail successfully",
                data: data
            })
        })
    })

}

const getAllOrderDetailOfOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Ordere ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId)
        .populate("orderDetails")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all OrderDetail of Order successfully",
                data: data
            })
        })
}

const getOrderDetailById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderDetail ID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
   orderDetailModel.findById(orderDetailId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail OrderDetail successfully",
            data: data
        })
    })
}

const updateOrderDetail = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderDetail ID không hợp lệ"
        })
    }
    if (body.product && !mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Product không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateOrderDetail = {}

    if(body.product !== undefined) {
        updateOrderDetail.product = body.product
    }


    orderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update OrderDetail successfully",
            data: data
        })
    })
}

const deleteOrderDetail = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderDetailId = request.params.orderDetailId;
    const orderId=request.params.orderId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderDetailId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderDetailModel.findByIdAndDelete(orderDetailId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        orderModel.findByIdAndUpdate(orderId, {
            $pull: { orders: orderId }
        }, (err, updateOrder) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete OrderDetail successfully"
            })
        })
    })
}
module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}