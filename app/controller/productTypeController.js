// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import Module ProductType Model
const productTypeModel = require("../model/productTypeModel");

const CreateProductType = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    // B2: Validate dữ liệu

    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }
    if(!body.description) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Description không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }

    productTypeModel.create(newProductType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create ProductType successfully",
            data: data
        })
    })
}

const GetAllProductType = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    productTypeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Product Type successfully",
            data: data
        })
    })
}

const GetProductTypeByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productTypeId = request.params.productTypeId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    productTypeModel.findById(productTypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail product Type successfully",
            data: data
        })
    })
}

const UpdateProductType = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productTypeId = request.params.productTypeId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductTypeID không hợp lệ"
        })
    }

    if(body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }

    if(body.description !== undefined && body.description.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Description không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateProductType = {}

    if(body.name !== undefined) {
        updateProductType.name = body.name
    }

    if(body.description !== undefined) {
        updateProductType.description = body.description
    }

    productTypeModel.findByIdAndUpdate(productTypeId, updateProductType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Product Type successfully",
            data: data
        })
    })
}

const DeleteProductType = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productTypeId = request.params.productTypeId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productTypeID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete productType successfully"
        })
    })
}
module.exports = {
    CreateProductType,
    GetAllProductType,
    GetProductTypeByID,
    UpdateProductType,
    DeleteProductType
}