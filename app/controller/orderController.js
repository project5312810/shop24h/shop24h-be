// Import thư viện Mongoose
const { request, response } = require("express");
const mongoose = require("mongoose");
// Import Module product Model
const orderModel = require("../model/orderModel");
const customerModel= require("../model/customerModel");

const createOrderOfCustomer=(request,response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId=request.params.customerId;
    const body = request.body;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer ID is not valid"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        shippedDate: body.shippedDate,
        note: body.note
    }

    orderModel.create(newOrder,(error,data)=>{
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //Them ID cua order moi vao mang oders cuar customer da chon
        customerModel.findByIdAndUpdate(customerId,{
            $push:{orders: data._id}
        },(err,updateCustomer)=>{
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status: "Create Order successfully",
                data: data
            })
        })
    })

}

const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    orderModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Ỏder Type successfully",
            data: data
        })
    })
}

const getAllOrderOfCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customere ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all order of customer successfully",
                data: data
            })
        })
}

const getOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
   orderModel.findById(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Order Type successfully",
            data: data
        })
    })
}

const updateOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateOrder = {}

    if(body.shippedDate !== undefined) {
        updateOrder.shippedDate = body.shippedDate
    }

    if(body.note !== undefined) {
        updateOrder.note = body.note
    }

    orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Order Type successfully",
            data: data
        })
    })
}

const deleteOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    const customerId=request.params.customerId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        customerModel.findByIdAndUpdate(customerId, {
            $pull: { orders: orderId }
        }, (err, updateCustomer) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete Order successfully"
            })
        })
    })
}
module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrder,
    deleteOrder
}