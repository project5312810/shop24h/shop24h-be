// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import Module Customer Model
const customerModel = require("../model/customerModel");

const createCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;

    if(!body.fullName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(!body.phone) {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    if(!body.email) {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email
    }

    customerModel.create(newCustomer, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create Customer successfully",
            data: data
        })
    })
}
    
const getAllCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    customerModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Customer Type successfully",
            data: data
        })
    })
}

const getCustomerById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    customerModel.findById(customerId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Customer Type successfully",
            data: data
        })
    })
}

const updateCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }

    if(body.fullName !== undefined && body.fullName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    if(body.email !== undefined && body.email.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateCustomer = {}

    if(body.fullName !== undefined) {
        updateCustomer.fullName = body.fullName
    }

    if(body.phone !== undefined) {
        updateCustomer.phone = body.phone
    }
    if(body.email !== undefined) {
        updateCustomer.email = body.email
    }

    customerModel.findByIdAndUpdate(customerId, updateCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Customer Type successfully",
            data: data
        })
    })
}

const deleteCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "customerId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    customerModel.findByIdAndDelete(customerId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete Customer successfully"
        })
    })
}
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}