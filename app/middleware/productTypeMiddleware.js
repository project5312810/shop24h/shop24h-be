const createProductTypeMiddleware = (request, response, next) => {
    console.log("Create ProductType Middleware");
    next();
}
const getAllProductTypeMiddleware = (request, response, next) => {
    console.log("Get ALL ProductType Middleware");
    next();
}
const getProductTypeByIDMiddleware = (request, response, next) => {
    console.log("Get Detail ProductType Middleware");
    next();
}

const updateProductTypeMiddleware = (request, response, next) => {
    console.log("Update ProductType Middleware");
    next();
}

const deleteProductTypeMiddleware = (request, response, next) => {
    console.log("Delete ProductType Middleware");
    next();
}

module.exports = {
    createProductTypeMiddleware,
    getAllProductTypeMiddleware,
    getProductTypeByIDMiddleware,
    updateProductTypeMiddleware,
    deleteProductTypeMiddleware
}