const createOrderDetailMiddleware = (request, response, next) => {
    console.log("Create OrderDetail Middleware");
    next();
}

const getAllOrderDetailOfOrderMiddleware = (request, response, next) => {
    console.log("Get Detail OrderDetail Middleware");
    next();
}

const getOrderDetailByIDMiddleware = (request, response, next) => {
    console.log("Get OrderDetail By ID Middleware");
    next();
}

const updateOrderDetailMiddleware = (request, response, next) => {
    console.log("Update OrderDetail Middleware");
    next();
}

const deleteOrderDetailMiddleware = (request, response, next) => {
    console.log("Delete OrderDetail Middleware");
    next();
}

module.exports = {
    createOrderDetailMiddleware,
    getAllOrderDetailOfOrderMiddleware,
    getOrderDetailByIDMiddleware,
    updateOrderDetailMiddleware,
    deleteOrderDetailMiddleware
}