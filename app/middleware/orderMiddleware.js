const createOrderMiddleware = (request, response, next) => {
    console.log("Create Order Middleware");
    next();
}
const getAllOrderMiddleware = (request, response, next) => {
    console.log("Get ALL Order Middleware");
    next();
}
const getDetailReviewMiddleware = (request, response, next) => {
    console.log("Get Detail Order Middleware");
    next();
}

const getOrderByIDMiddleware = (request, response, next) => {
    console.log("Get Order By ID Middleware");
    next();
}

const updateOrderMiddleware = (request, response, next) => {
    console.log("Update Order Middleware");
    next();
}

const deleteOrderMiddleware = (request, response, next) => {
    console.log("Delete Order Middleware");
    next();
}

module.exports = {
    createOrderMiddleware,
    getAllOrderMiddleware,
    getDetailReviewMiddleware,
    getOrderByIDMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}