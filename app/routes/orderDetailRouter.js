// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import order middleware
const orderDetailMiddleware = require("../middleware/orderDetailMiddleware");

// Import order controller
const orderDetailController = require("../controller/orderDetailController");

router.post("/orders/:orderId/orderDetails", orderDetailMiddleware.createOrderDetailMiddleware, orderDetailController.createOrderDetailOfOrder);

router.get("/orders/:orderId/orderDetails", orderDetailMiddleware.getAllOrderDetailOfOrderMiddleware, orderDetailController.getAllOrderDetailOfOrder);

router.get("/orderDetails/:orderDetailId", orderDetailMiddleware.getOrderDetailByIDMiddleware, orderDetailController.getOrderDetailById);

router.put("/orderDetails/:orderDetailId", orderDetailMiddleware.updateOrderDetailMiddleware, orderDetailController.updateOrderDetail);

router.delete("/orders/:orderId/orderDetails/:orderDetailId", orderDetailMiddleware.deleteOrderDetailMiddleware, orderDetailController.deleteOrderDetail)

module.exports = router;