// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import Customer middleware
const customerMiddleware = require("../middleware/customerMiddleware");

// Import Customer controller
const customerController = require("../controller/customerController");

router.post("/customers", customerMiddleware.createCustomerMiddleware, customerController.createCustomer);

router.get("/customers", customerMiddleware.getAllCustomerMiddleware, customerController.getAllCustomer);

router.get("/customers/:customerId", customerMiddleware.getCustomerByIDMiddleware, customerController.getCustomerById);

router.put("/customers/:customerId", customerMiddleware.updateCustomerMiddleware, customerController.updateCustomer);

router.delete("/customers/:customerId", customerMiddleware.deleteCustomerMiddleware, customerController.deleteCustomer)

module.exports = router;