// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import order middleware
const orderMiddleware = require("../middleware/orderMiddleware");

// Import order controller
const orderController = require("../controller/orderController");

router.post("/customers/:customerId/orders", orderMiddleware.createOrderMiddleware, orderController.createOrderOfCustomer);

router.get("/orders", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder);

router.get("/customers/:customerId/orders", orderMiddleware.createOrderMiddleware, orderController.getAllOrderOfCustomer);

router.get("/orders/:orderId", orderMiddleware.getOrderByIDMiddleware, orderController.getOrderById);

router.put("/orders/:orderId", orderMiddleware.updateOrderMiddleware, orderController.updateOrder);

router.delete("/customers/:customerId/orders/:orderId", orderMiddleware.deleteOrderMiddleware, orderController.deleteOrder)

module.exports = router;