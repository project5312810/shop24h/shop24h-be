// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import Product middleware
const productMiddleware = require("../middleware/productMiddleware");

// Import Product controller
const productController = require("../controller/productController");

router.post("/products", productMiddleware.createProductMiddleware, productController.createProduct);

router.get("/products", productMiddleware.getAllProductMiddleware, productController.getAllProduct);

router.get("/products/:productId", productMiddleware.getProductByIDMiddleware, productController.getProductById);

router.put("/products/:productId", productMiddleware.updateProductMiddleware, productController.updateProduct);

router.delete("/products/:productId", productMiddleware.deleteProductMiddleware, productController.deleteProduct)

module.exports = router;