// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import ProductType middleware
const productTypeMiddleware = require("../middleware/productTypeMiddleware");

// Import ProductType controller
const productTypeController = require("../controller/productTypeController");

router.post("/productTypes", productTypeMiddleware.createProductTypeMiddleware, productTypeController.CreateProductType);

router.get("/productTypes", productTypeMiddleware.getAllProductTypeMiddleware, productTypeController.GetAllProductType);

router.get("/productTypes/:productTypeId", productTypeMiddleware.getProductTypeByIDMiddleware, productTypeController.GetProductTypeByID);

router.put("/productTypes/:productTypeId", productTypeMiddleware.updateProductTypeMiddleware, productTypeController.UpdateProductType);

router.delete("/productTypes/:productTypeId", productTypeMiddleware.deleteProductTypeMiddleware, productTypeController.DeleteProductType)

module.exports = router;