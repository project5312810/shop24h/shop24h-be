// Khai báo thư viên Express
const express = require("express");
// Khai báo thư viện Mongoose
const mongoose = require("mongoose");
// Khởi tạo app express
const app = express();
const cors = require('cors')
// Khai báo cổng chạy app 
const port = 8000;
// Cấu hình request đọc được body json
app.use(express.json());

//khai bao model
const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter");
const orderDetailRouter = require("./app/routes/orderDetailRouter");
//connect moongose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h", (error) => {
    if (error) throw error;
    console.log("Connect MongoDB successfully!");
})
app.use(cors());
// App sử dụng router
app.use("/api", productTypeRouter);
app.use("/api", productRouter);
app.use("/api", customerRouter);
app.use("/api", orderRouter);
app.use("/api", orderDetailRouter);


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})